#ifndef RUNASROBOT_H
#define RUNASROBOT_H

#include "locate.h"

class RunAsRobot
{
public:
    RunAsRobot(int id, char robotIP[], char hostIP[], int pushFor);
};

#endif // RUNASROBOT_H
