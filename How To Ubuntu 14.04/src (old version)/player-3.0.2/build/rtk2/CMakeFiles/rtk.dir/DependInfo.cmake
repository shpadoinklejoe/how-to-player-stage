# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/jrgunderson/src/player-3.0.2/rtk2/rtk.c" "/home/jrgunderson/src/player-3.0.2/build/rtk2/CMakeFiles/rtk.dir/rtk.o"
  "/home/jrgunderson/src/player-3.0.2/rtk2/rtk_canvas.c" "/home/jrgunderson/src/player-3.0.2/build/rtk2/CMakeFiles/rtk.dir/rtk_canvas.o"
  "/home/jrgunderson/src/player-3.0.2/rtk2/rtk_canvas_movie.c" "/home/jrgunderson/src/player-3.0.2/build/rtk2/CMakeFiles/rtk.dir/rtk_canvas_movie.o"
  "/home/jrgunderson/src/player-3.0.2/rtk2/rtk_fig.c" "/home/jrgunderson/src/player-3.0.2/build/rtk2/CMakeFiles/rtk.dir/rtk_fig.o"
  "/home/jrgunderson/src/player-3.0.2/rtk2/rtk_menu.c" "/home/jrgunderson/src/player-3.0.2/build/rtk2/CMakeFiles/rtk.dir/rtk_menu.o"
  "/home/jrgunderson/src/player-3.0.2/rtk2/rtk_region.c" "/home/jrgunderson/src/player-3.0.2/build/rtk2/CMakeFiles/rtk.dir/rtk_region.o"
  "/home/jrgunderson/src/player-3.0.2/rtk2/rtk_table.c" "/home/jrgunderson/src/player-3.0.2/build/rtk2/CMakeFiles/rtk.dir/rtk_table.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  ".."
  "."
  "libplayercore"
  "/usr/include/libgnomecanvas-2.0"
  "/usr/include/pango-1.0"
  "/usr/include/gail-1.0"
  "/usr/include/libart-2.0"
  "/usr/include/gtk-2.0"
  "/usr/include/glib-2.0"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/freetype2"
  "/usr/include/atk-1.0"
  "/usr/lib/x86_64-linux-gnu/gtk-2.0/include"
  "/usr/include/cairo"
  "/usr/include/gdk-pixbuf-2.0"
  "/usr/include/gio-unix-2.0"
  "/usr/include/pixman-1"
  "/usr/include/libpng12"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
