# CMake generated Testfile for 
# Source directory: /home/jrgunderson/src/stage
# Build directory: /home/jrgunderson/src/stage/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(libstage)
SUBDIRS(examples)
SUBDIRS(assets)
SUBDIRS(worlds)
SUBDIRS(libstageplugin)
