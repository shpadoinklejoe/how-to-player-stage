# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/jrgunderson/src/player/examples/plugins/opaquedriver/opaque.c" "/home/jrgunderson/src/player/build/examples/plugins/opaquedriver/CMakeFiles/opaque_client.dir/opaque.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "PLAYER_UNIX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/jrgunderson/src/player/build/client_libs/libplayerc/CMakeFiles/playerc.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayerinterface/CMakeFiles/playerinterface.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayercommon/CMakeFiles/playercommon.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayerwkb/CMakeFiles/playerwkb.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayerjpeg/CMakeFiles/playerjpeg.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  ".."
  "."
  "libplayercore"
  "../client_libs"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
