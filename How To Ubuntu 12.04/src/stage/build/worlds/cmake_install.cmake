# Install script for directory: /home/jrgunderson/src/stage/worlds

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "RELEASE")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/stage/worlds" TYPE FILE FILES
    "/home/jrgunderson/src/stage/worlds/camera.cfg"
    "/home/jrgunderson/src/stage/worlds/wavefront.cfg"
    "/home/jrgunderson/src/stage/worlds/vfh.cfg"
    "/home/jrgunderson/src/stage/worlds/roomba.cfg"
    "/home/jrgunderson/src/stage/worlds/test.cfg"
    "/home/jrgunderson/src/stage/worlds/uoa_robotics_lab.cfg"
    "/home/jrgunderson/src/stage/worlds/lsp_test.cfg"
    "/home/jrgunderson/src/stage/worlds/simple.cfg"
    "/home/jrgunderson/src/stage/worlds/wifi.cfg"
    "/home/jrgunderson/src/stage/worlds/wavefront-remote.cfg"
    "/home/jrgunderson/src/stage/worlds/nd.cfg"
    "/home/jrgunderson/src/stage/worlds/amcl-sonar.cfg"
    "/home/jrgunderson/src/stage/worlds/everything.cfg"
    "/home/jrgunderson/src/stage/worlds/autolab.cfg"
    "/home/jrgunderson/src/stage/worlds/mbicp.cfg"
    "/home/jrgunderson/src/stage/worlds/sensor_noise_module_demo.world"
    "/home/jrgunderson/src/stage/worlds/uoa_robotics_lab.world"
    "/home/jrgunderson/src/stage/worlds/wifi.world"
    "/home/jrgunderson/src/stage/worlds/pioneer_flocking.world"
    "/home/jrgunderson/src/stage/worlds/SFU.world"
    "/home/jrgunderson/src/stage/worlds/autolab.world"
    "/home/jrgunderson/src/stage/worlds/mbicp.world"
    "/home/jrgunderson/src/stage/worlds/roomba.world"
    "/home/jrgunderson/src/stage/worlds/everything.world"
    "/home/jrgunderson/src/stage/worlds/large.world"
    "/home/jrgunderson/src/stage/worlds/pioneer_walle.world"
    "/home/jrgunderson/src/stage/worlds/sensor_noise_demo.world"
    "/home/jrgunderson/src/stage/worlds/fasr2.world"
    "/home/jrgunderson/src/stage/worlds/simple.world"
    "/home/jrgunderson/src/stage/worlds/fasr_plan.world"
    "/home/jrgunderson/src/stage/worlds/camera.world"
    "/home/jrgunderson/src/stage/worlds/lsp_test.world"
    "/home/jrgunderson/src/stage/worlds/fasr.world"
    "/home/jrgunderson/src/stage/worlds/map.inc"
    "/home/jrgunderson/src/stage/worlds/pioneer.inc"
    "/home/jrgunderson/src/stage/worlds/uoa_robotics_lab_models.inc"
    "/home/jrgunderson/src/stage/worlds/pantilt.inc"
    "/home/jrgunderson/src/stage/worlds/objects.inc"
    "/home/jrgunderson/src/stage/worlds/chatterbox.inc"
    "/home/jrgunderson/src/stage/worlds/beacons.inc"
    "/home/jrgunderson/src/stage/worlds/hokuyo.inc"
    "/home/jrgunderson/src/stage/worlds/ubot.inc"
    "/home/jrgunderson/src/stage/worlds/sick.inc"
    "/home/jrgunderson/src/stage/worlds/irobot.inc"
    "/home/jrgunderson/src/stage/worlds/walle.inc"
    "/home/jrgunderson/src/stage/worlds/worldgen.sh"
    "/home/jrgunderson/src/stage/worlds/cfggen.sh"
    "/home/jrgunderson/src/stage/worlds/test.sh"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  INCLUDE("/home/jrgunderson/src/stage/build/worlds/benchmark/cmake_install.cmake")
  INCLUDE("/home/jrgunderson/src/stage/build/worlds/bitmaps/cmake_install.cmake")
  INCLUDE("/home/jrgunderson/src/stage/build/worlds/wifi/cmake_install.cmake")

ENDIF(NOT CMAKE_INSTALL_LOCAL_ONLY)

