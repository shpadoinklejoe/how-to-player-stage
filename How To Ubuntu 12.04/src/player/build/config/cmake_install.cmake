# Install script for directory: /home/jrgunderson/src/player/config

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr/local")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "1")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "samplecfg")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/player/config" TYPE FILE FILES
    "/home/jrgunderson/src/player/config/afsm.cfg"
    "/home/jrgunderson/src/player/config/afsm.eps"
    "/home/jrgunderson/src/player/config/amigobot.cfg"
    "/home/jrgunderson/src/player/config/amigobot_tcp.cfg"
    "/home/jrgunderson/src/player/config/amtecM5.cfg"
    "/home/jrgunderson/src/player/config/b21r_rflex_lms200.cfg"
    "/home/jrgunderson/src/player/config/cvcam.cfg"
    "/home/jrgunderson/src/player/config/dummy.cfg"
    "/home/jrgunderson/src/player/config/erratic.cfg"
    "/home/jrgunderson/src/player/config/hokuyo_aist.cfg"
    "/home/jrgunderson/src/player/config/iwspy.cfg"
    "/home/jrgunderson/src/player/config/joystick.cfg"
    "/home/jrgunderson/src/player/config/kinect.cfg"
    "/home/jrgunderson/src/player/config/lms400.cfg"
    "/home/jrgunderson/src/player/config/magellan.cfg"
    "/home/jrgunderson/src/player/config/mapfile.cfg"
    "/home/jrgunderson/src/player/config/mbicp.cfg"
    "/home/jrgunderson/src/player/config/nomad.cfg"
    "/home/jrgunderson/src/player/config/obot.cfg"
    "/home/jrgunderson/src/player/config/passthrough.cfg"
    "/home/jrgunderson/src/player/config/phidgetIFK.cfg"
    "/home/jrgunderson/src/player/config/phidgetRFID.cfg"
    "/home/jrgunderson/src/player/config/pioneer.cfg"
    "/home/jrgunderson/src/player/config/pioneer_rs4euze.cfg"
    "/home/jrgunderson/src/player/config/pointcloud3d.cfg"
    "/home/jrgunderson/src/player/config/readlog.cfg"
    "/home/jrgunderson/src/player/config/rfid.cfg"
    "/home/jrgunderson/src/player/config/roomba.cfg"
    "/home/jrgunderson/src/player/config/searchpattern.cfg"
    "/home/jrgunderson/src/player/config/searchpattern_symbols.ps"
    "/home/jrgunderson/src/player/config/segwayrmp.cfg"
    "/home/jrgunderson/src/player/config/service_adv.cfg"
    "/home/jrgunderson/src/player/config/simple.cfg"
    "/home/jrgunderson/src/player/config/sphere.cfg"
    "/home/jrgunderson/src/player/config/umass_ATRVJr.cfg"
    "/home/jrgunderson/src/player/config/umass_ATRVMini.cfg"
    "/home/jrgunderson/src/player/config/umass_reb.cfg"
    "/home/jrgunderson/src/player/config/urglaser.cfg"
    "/home/jrgunderson/src/player/config/vfh.cfg"
    "/home/jrgunderson/src/player/config/wavefront.cfg"
    "/home/jrgunderson/src/player/config/wbr914.cfg"
    "/home/jrgunderson/src/player/config/writelog.cfg"
    "/home/jrgunderson/src/player/config/wsn.cfg"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "samplecfg")

