# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/jrgunderson/src/player/examples/libplayerc++/wallfollow.cc" "/home/jrgunderson/src/player/build/examples/libplayerc++/CMakeFiles/wallfollow.dir/wallfollow.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "PLAYER_UNIX"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/jrgunderson/src/player/build/client_libs/libplayerc++/CMakeFiles/playerc++.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/client_libs/libplayerc/CMakeFiles/playerc.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayerinterface/CMakeFiles/playerinterface.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayercommon/CMakeFiles/playercommon.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayerjpeg/CMakeFiles/playerjpeg.dir/DependInfo.cmake"
  "/home/jrgunderson/src/player/build/libplayerwkb/CMakeFiles/playerwkb.dir/DependInfo.cmake"
  )
